package time.space.array;

import java.util.Arrays;

public class arryTest {
    public static void main(String[] args) {
//        char[][] board={
//  {'8','3','.','.','7','.','.','.','.'},
//  {'6','.','.','1','9','5','.','.','.'},
//  {'.','9','8','.','.','.','.','6','.'},
//  {'8','.','.','.','6','.','.','.','3'},
//  {'4','.','.','8','.','3','.','.','1'},
//  {'7','.','.','.','2','.','.','.','6'},
//  {'.','6','.','.','.','.','2','8','.'},
//  {'.','.','.','4','1','9','.','.','5'},
//  {'.','.','.','.','8','.','.','7','9'}
//};

        char[][] board=  {
  {'5','3','.','.','7','.','.','.','.'},
  {'6','.','.','1','9','5','.','.','.'},
  {'.','9','8','.','.','.','.','6','.'},
  {'8','.','.','.','6','.','.','.','3'},
  {'4','.','.','8','.','3','.','.','1'},
  {'7','.','.','.','2','.','.','.','6'},
  {'.','6','.','.','.','.','2','8','.'},
  {'.','.','.','4','1','9','.','.','5'},
  {'.','.','.','.','8','.','.','7','9'}
};

        System.out.println(isValidSudoku(board));
      
    }
    public static  boolean isValidSudoku(char[][] board) {
        int[] arr=new int[board[0].length];
        int index=0;

        boolean flag1=false;


        for(int i=0;i<board.length;i++){
            for(int j=0;j<board[i].length;j++){
                if(!".".equals(String.valueOf(board[i][j]))){
                    arr[index]=Integer.parseInt(String.valueOf(board[i][j]));
                    index++;
                }
            }
            Arrays.sort(arr);
            for(int ii=0;ii<arr.length-1;ii++){
                if(arr[ii]==arr[ii+1]){
                    flag1= true;
                    break;
                }
                index=0;
            }
            if(flag1){
                return false;
            }
        }


        for(int j=0;j<board[0].length;j++){
            for(int i=0;j<board.length;i++){

                if(!".".equals(String.valueOf(board[i][j]))){
                    arr[index]=Integer.parseInt(String.valueOf(board[i][j]));
                    index++;
                }
            }
            Arrays.sort(arr);
            for(int ii=0;ii<arr.length-1;ii++){
                if(arr[ii]==arr[ii+1]){
                    flag1= true;
                    break;
                }
                index=0;
            }
            if(flag1){
                return false;
            }

        }

        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                for(int k=i*3;k<i*3+3;k++){
                    for(int t=j*3;t<j*3+3;t++){
                        if(!".".equals(String.valueOf(board[k][t]))){
                            arr[index]=Integer.parseInt(String.valueOf(board[k][t]));
                            index++;
                        }
                    }
                }
                Arrays.sort(arr);
                for(int ii=0;ii<arr.length-1;ii++){
                    if(arr[ii]==arr[ii+1]){
                        flag1= true;
                        break;
                    }
                    index=0;
                }
                if(flag1){
                    return false;
                }
            }
        }

        return true;

    }
}
