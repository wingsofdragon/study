package time.space.sort.insertsort;

public class InsertSort {

    /*
    * 简单插入排序:假设索引i处开始,前面的数组都已经排好序,则只需要将i索引及后面的数,挨个插入前面已经排好的序列即可.
    * 1.从索引为1处开始,(前面只有0索引一个值,即其是有序的)
    * 2.若到了i索引,i索引处的值都大于了i-1处的值,这种是不需要排的(前面的值都比i处的小).故只有arr[i]<arr[i-1]才需要执行插入操作.
    *
    *
    *
    * */
    public  static void insertSort(int[] arr){
        int temp=0;

        for (int i = 1; i < arr.length; i++) {
            temp=arr[i];  //记录下索引为i的值
            if(arr[i]<arr[i-1]){//arr[i]<arr[i-1]才需要执行插入操作.

                for(int j=i;j>=1&&arr[j]<arr[j-1];j--){//索引j必须大于等于1.(此时已经把数组走完了,再走没数了.也就是j比前面所有的值都小)
                    arr[j]=arr[j-1];
                    arr[j-1]=temp;    //将这个循环中固定的arr[i]的值一步一步的往前放,直到arr[i]的值大于等于了某个值时停下来
                }

            }
        }


    }
}
