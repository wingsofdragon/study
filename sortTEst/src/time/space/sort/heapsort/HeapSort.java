package time.space.sort.heapsort;

public class HeapSort {


    public static void heapSort(int[] arr, int n){
        buildHeap(arr,n);//构堆
        for (int i = n-1; i >=0 ; i--) {
            swap(arr,0,i);//将最大的数放在堆末尾
            heapify(arr,i,0);//每次默认减少一个节点(这个节点存的是本次的堆结构中最大的数),再重构剩下的堆结构
        }
    }
    public static void buildHeap(int[] tree, int n){//从树的倒数第二层开始往上做heapify,由于heapify由递归的特性,每次heapify后,i节点下面的都为堆结构(太小的数会被一直往下沉,直到满足堆结构)
        int last_node=n-1;
        int last_parent=(last_node-1)/2;
        for (int i = last_parent; i >=0 ; i--) {
            heapify(tree,n,i);
        }
    }

    public  static void heapify(int[] tree , int n,int i){//i节点的父节点(i-1)/2        两个子i节点是(2*i+1    和     2*i+2)
        int max=i;
        int c1=2*i+1;
        int c2=2*i+2;
        if(i>=n){return;}//递归出口(可以改进)
        if(c1<n&&tree[c1]>tree[max]){//两个子节点与i节点比较大小,大于i节点就记录这个节点为max
            max=c1;
        }
        if(c2<n&&tree[c2]>tree[max]){//
            max=c2;
        }
        if(max!=i){//如果需要交换,则交换,并对动过的子节点往下做比较(并递归),直到往下放的数据比两个子子节点小的情况停止
            swap(tree,max,i);//如果最大的不是i,则将i和max记录的子节点值交换,保证堆特性
            heapify(tree,n,max);//交换以后对交换过的子节点做heapify(通过递归保证,如果上面的数很小的情况,一直把这个数往下面沉,直到下面的数都比它小(满足堆特性))
        }
    }

    private static void swap(int[] tree, int max, int i) {//交换数组中的两个索引处的值
        int temp=tree[i];
        tree[i]=tree[max];
        tree[max]=temp;
    }


}
