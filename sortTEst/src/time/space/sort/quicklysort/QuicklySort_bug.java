package time.space.sort.quicklysort;

public class QuicklySort_bug {


    public  void quicklySort(int[] arr,int base,int tail){
        int left=base+1;
        int right=tail;
        int temp=0;
        if(right==left){
            if (arr[right]<arr[base]){
                temp=arr[base];
                arr[base]=arr[right];
                arr[right]=temp;
                return;
            }
        }else{

            while (left<=right) {
                while (true) {
                    if (left < tail && arr[left] <= arr[base]) {
                        left++;
                    } else {
                        break;
                    }
                }
                while (true) {
                    if (right > base && arr[right] >= arr[base]) {
                        right--;
                    } else {
                        break;
                    }
                }

//                if(left==right){
//                    break;
//                }           //加这句和下面的break是一样的,不能把这句加后面这个if语句后,(不然会立马跳出,right还没来得及判断现在锁定的值是否需要锁定)

                if (left < right){
                    temp = arr[left];
                    arr[left] = arr[right];
                    arr[right] = temp;
                    left++;////-----
                    right--;////=======//这里如果执行后刚好left=right,不能立马跳出,还需要让left和right再找一次(主要是让right判断这个值是否大于参考值,从而决定--还是记录这个值)
                            /////,再跳出.right不然这个值的大小会是未知的,并且会与base处的值交换,此时排序错误
                } else {
                    break;//上面的if执行完后出现left==right时候不能马上跳出,还需要让left和right再找一次.
                           //不加的话会在当left=right时候,并且left和right不会再改变(left到达最左边:即left已经等于tail,,,并且right记录了left==right处的值:这个值小于base处的值)
                        //此时right和left永远相等,死循环!!!
                }



            }


            temp = arr[base];
            arr[base] = arr[right];
            arr[right] = temp;


            if (right - 1 > base) {
                quicklySort(arr, base, right - 1);
            }
            if (right + 1 <tail) {
                quicklySort(arr, right + 1, tail);
            }
        }
    }
}
