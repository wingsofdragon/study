package time.space.sort.sortTest;

public class SortWrongException extends Exception{
    public SortWrongException() {

    }

    public SortWrongException(String message) {
        super(message);
    }
}
