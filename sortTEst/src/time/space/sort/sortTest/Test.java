package time.space.sort.sortTest;

import time.space.sort.bubblingsort.BubblingSort;
import time.space.sort.heapsort.HeapSort;
import time.space.sort.insertsort.InsertSort;
import time.space.sort.quicklysort.QuicklySort;

import java.util.Arrays;
import java.util.Random;


public class Test {
    public static void main(String[] args) {
        Random rd = new Random();
        int[] arr=new int[19999];
        for (int i = 0; i <19999 ; i++) {
            int t = rd.nextInt(1000);
            arr[i]=t;
        }
        System.out.println(Arrays.toString(arr));


        long t1 = System.currentTimeMillis();
//        InsertSort.insertSort(arr);
        long t2 = System.currentTimeMillis();

        QuicklySort.quicklySort(arr,0,arr.length-1);
        long t3 = System.currentTimeMillis();


//        BubblingSort.bubblingSort(arr);
        long t4 = System.currentTimeMillis();



//        HeapSort.heapSort(arr, arr.length);
        long t5 = System.currentTimeMillis();
        System.out.println("cha:"+(t2-t1)+"       "+"\nqu:"+(t3-t2)+"       "+"\nmp:"+(t4-t3)+"       " +"\nhs:"+(t5-t4)+"       ");


//        System.out.println(Arrays.toString(arr));


        try {
            ChcekSort.checkSort(arr);
        } catch (SortWrongException e) {
            e.printStackTrace();
        }
    }
}
